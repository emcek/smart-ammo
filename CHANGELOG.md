## Smart Ammo for OpenMW-Lua Changelog

#### 1.0

Initial mod release.

Features:

* Auto-equips the correct ammo when a marksman-type weapon is equipped...
* ... and when your current ammo runs out, it's auto-restocked with another of the correct type (if you have any).
* Settings menu allows toggling messages on or off (on by default).

[Download Link](https://gitlab.com/modding-openmw/smart-ammo/-/packages/6910444) | [Nexus](https://www.nexusmods.com/morrowind/mods/51274)
